#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	// functions for internal-workings
	bool ShiftRight(int atIndex)
	{
		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}
		for  (int i = atIndex+1 ; i < m_itemCount; i++)
		{
			m_arr[i] = m_arr[i + 1];
			
		}
		 return true; // placeholder
	}

	bool ShiftLeft(int atIndex)
	{

		if (atIndex < 0 || atIndex > m_itemCount)
		{
			return false;
		}

		for (int i = atIndex + 1; i < m_itemCount; i++)
		{
			m_arr[i - 1] = m_arr[i];
		}

		return true;
	}

public:
	List()
	{
		m_itemCount = 0;
	}

	~List()
	{
		
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount; // placeholder
	}

	bool    IsEmpty() const
	{
		return (m_itemCount == 0); // placeholder
	}

	bool    IsFull() const
	{
		return (m_itemCount == ARRAY_SIZE);// placeholder
	}

	bool    PushFront(const T& newItem)
	{
		if (IsFull()) {

			return false;
		}
		m_arr[0] = newItem;
		m_itemCount++;
		return true;
	}


	bool    PushBack(const T& newItem)
	{
		if (IsFull())
		{
			return false;
		}

		m_arr[m_itemCount] = newItem;
		m_itemCount++;
		return true;
	}

	bool    Insert(int atIndex, const T& item)
	{
		if (IsFull())
		{
			return false;
		}
		else 
		{
			ShiftRight(atIndex);
			m_arr[atIndex] = item;
			m_itemCount++;
			return true;
		}// placeholder
	}

	bool    PopFront()
	{
		if (IsEmpty()) { return false; }

		ShiftLeft(0);
		m_itemCount--;
		return true;

	}

	bool    PopBack()
	{
		if (IsEmpty()) { return false; }
		m_itemCount--;
		return true;

	}

	bool    RemoveItem(const T& item)
	{
		for (int i = 0; i < m_itemCount; i++)
		{
			if (m_arr[i] ==item)
			{
				ShiftLeft();
				m_itemCount--;
				
			}
			
			return true;
		}
		// placeholder
	}

	bool    RemoveAtIndex(int atIndex)
	{
		if (atIndex > 0 || atIndex <= m_itemCount) 
		
		
		{
			ShiftLeft(atIndex);
			m_itemCount--;
			
		}
		return true;
		
	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)

	{
		if (atIndex<0||atIndex>= m_itemCount)
		{
			return nullptr;

		}
		else
		{
			return &m_arr[atIndex];
		}
		; // placeholder
	}

	T*      GetFront()
	{
		if (IsEmpty())
		{
			return nullptr;
		}
		else
		{
			return &m_arr[0];
		}// placeholder
	}

	T*      GetBack()
	{
		if (IsEmpty()) 
		{
			return nullptr;
		}
		else
		{
			return &m_arr[m_itemCount];
		}
		// placeholder
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		int count = 0;
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			if (m_arr[i] == item)
				count++;
		}
		return count;
		// placeholder
	}

	bool    Contains(const T& item) const
	{

		return(GetCountOf(item) > 0);
	}
	friend class Tester;
};


#endif
