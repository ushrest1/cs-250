#include "Tester.hpp"

void Tester::RunTests()
{
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	// Put tests here
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;
	{
		cout << "Test 1" << endl;
		List<string> testlist;
		cout << "create a list and push back A,B,C then shift right(1), Get_0 should return A,Get_2 should return B,Get_3 should return C";
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.ShiftRight(1);

		string expectedValue_0 = "A";
		string expectedValue_2 = "B";
		string expectedValue_3 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_2 = testlist.Get(2);
		string* actualValue_3 = testlist.Get(3);
		

		if (expectedValue_0== *actualValue_0 &&
			expectedValue_2== *actualValue_2 &&
			expectedValue_3== *actualValue_3)
		{
			cout <<"Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}
	// Put tests here
}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;
	{
		cout << "Test 1" << endl;
		List<string> testlist;
		cout << "create a list and push back A,B,C then shift right(1), Get_0 should return A,Get_1 should return C,";
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		testlist.ShiftLeft(1);

		string expectedValue_0 = "A";
		string expectedValue_1 = "C";
		

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		

		if (expectedValue_0 == *actualValue_0 &&
			expectedValue_1 == *actualValue_1 )
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

	// Put tests here

}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;

	{   // Test begin
		cout << endl << "Test 1" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin
		cout << endl << "Test 2" << endl;
		List<int> testList;

		testList.PushBack(1);

		int expectedSize = 1;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;
	{
		cout << endl << "Test 1" << endl;
		// Test 1
		List<int> testlist;

		bool expectedValue = true;
		bool actualValue = testlist.IsEmpty();

		cout << "Created empty list; IsEmpty() should be true." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	{
		// Test 2
		cout << endl << "Test 2" << endl;
		cout << "Prerequisites: PushBack()" << endl;
		List<int> testlist;

		testlist.PushBack(1);

		bool expectedValue = false;
		bool actualValue = testlist.IsEmpty();

		cout << "Created list, added 1 item, IsEmpty() should be false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

	// Test 3
	cout << endl << "Test 3 " << endl;
	cout << " Pushback() " << endl;

	List<int>testlist;
	testlist.PushBack(101);
	bool expectedValue = false;
	bool actualValue = testlist.IsEmpty();

	cout << "Created list, added 101 item, IsEmpty() should be false." << endl;
	if (expectedValue == actualValue)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}

}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;

	// Put tests here
	{

		cout << endl << "Test 1" << endl;
		//Test 1.
		List<int> testlist;

		bool expectedValue = false;
		bool actualValue = testlist.IsFull();
		cout << "Created empty list; Isfull() should be false." << endl;
		if (expectedValue == actualValue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	
		{
			cout << endl << "Test 2" << endl;
			//Test 2.
			List<int> testlist;
			for (int i = 0; i <= ARRAY_SIZE; i++)
			{
				testlist.PushBack(i);
			}

			bool expectedValue = true;
			bool actualValue = testlist.IsFull();

			cout << "Created 100 list ; Is full() should be false." << endl;
			if (expectedValue == actualValue)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}


		}

		{
			cout << endl << "Test 3" << endl;
			//Test 3.
			List<int> testlist;
			for (int i = 0; i <= 101; i++)
			{
				testlist.PushBack(i);

			}

			bool expectedValue = true;
			bool actualValue = testlist.IsFull();

			cout << "Created 101 list ; Is full() should be false." << endl;
			if (expectedValue == actualValue)
			{
				cout << "Pass" << endl;
			}
			else
			{
				cout << "Fail" << endl;
			}


		}


	}
}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;
	{
		cout << endl << "Test 1" << endl;
		//Test 1.
		List<string> testlist;

		testlist.PushFront("A");
		testlist.PushFront("B");
		testlist.PushFront("C");
		cout << "Test 1."<< endl;
		cout <<"create a list and push front A,B,C ,Get(0) should return C,Get(1) should return B,Get(2) should return A," << endl;
		string expectedvalue_0 = "C";
		string expectedvalue_1 = "B";
		string expectedvalue_2 = "A";


		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);
		

		if (*actualValue_0 == expectedvalue_0 &&
			*actualValue_1 == expectedvalue_1 &&
			*actualValue_2 == expectedvalue_2)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// Test 2
	
	cout << endl << "Test 2" << endl << endl;
	List <string>textlist;
	textlist.PushFront("B");
	textlist.PushFront("A");
	cout << endl << "Test 2, create a list and insert 2 item , size should return 2  " << endl;

	

	int expectedValue = 2;
	int actualValue = textlist.Size();

	if (expectedValue == actualValue)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}

	//Test 3
	
	cout << endl << "Test 3" << endl;
	cout << endl << "create a list and insert 100 K item ,Also push front A, B, size should return 100   " << endl;
	List <string>list;
	for (int i = 0; i < 100; i++)
	{
		list.PushFront("K");


	}
	list.PushFront("A");
	list.PushFront("B");
	int exceptvalue = 100;
	int actual = list.Size();
	if (exceptvalue == actual)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}


}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	cout << endl << "Test 1." << endl;
	//test 1.
	List <string> textlist;

	cout << "create a list and do nothing " << endl;
	bool expectedvalue = true;
	bool actualvalue = textlist.PushBack("A");

	cout << "create a list, PushBack 1 item, should be successful" << endl;
	if (expectedvalue == actualvalue)
	{
		cout << "Pass" << endl;
	}
	else
	{
		cout << "Fail" << endl;
	}
	// end test 1
	{
		cout << endl << "Test 2." << endl;
		//test 2.
		List <string> textlist;

		textlist.PushBack("B");
		textlist.PushBack("c");

		int expectedvalue = 2;
		int actualvalue = textlist.Size();

		cout << "create a list , push back 2 item " << endl;
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}
	// end test 2
	{
		// Test 3
		cout << endl << "Test 3" << endl;
		cout << "Prerequisite: Get()" << endl;

		List<string> testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");


		string expectedValues[3] = {
			"A",
			"B",
			"C"
		};
		string* actualValues[3] = {
			testlist.Get(0),
			testlist.Get(1),
			testlist.Get(2)
		};

		// Leave the test before we de-reference the pointers
		// so that it doesn't crash.
		if (actualValues[0] == nullptr || actualValues[1] == nullptr, actualValues[2] == nullptr)
		{
			cout << "Got nullptrs; avoid segfault. Returning" << endl;
			return;
		}

		if (*actualValues[0] == expectedValues[0] &&
			*actualValues[1] == expectedValues[1] &&
			*actualValues[2] == expectedValues[2])
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}

}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;
	{
		//Test 1
		cout << "Test 1.";
		cout << "create a list, do nothing , pop front should return false " << endl;
		List <string> testlist;
		bool expectoutput = false;
		bool actualoutput = testlist.PopFront();
		if (expectoutput == actualoutput)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}

	}

	{
		//Test 2
		cout << "test 2.";
		cout << "create a list pushback A,B,C ,popfront return true ,get front return B " << endl;
		List<string>testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedvalue = true;
		bool actualvalue = testlist.PopFront();

		string expected_0 = "B";
		string* actual_0 = testlist.Get(0);
		if (expectedvalue == actualvalue && expected_0== *actual_0)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}



	}
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;
	{
		//Test 1
		cout << "Test 1.";
		cout << "create a list, do nothing , pop back should return false " << endl;
		List <string> testlist;
		bool expectoutput = false;
		bool actualoutput = testlist.PopBack();
		if (expectoutput == actualoutput)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}

	}
	{
		//Test 2
		cout << "test 2.";
		cout << "create a list pushback A,B,C ,popback return true ,get front return B " << endl;
		List<string>testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		bool expectedvalue = true;
		bool actualvalue = testlist.PopBack();

		string expected_0 = "B";
		string* actual_0 = testlist.Get(0);
		if (expectedvalue == actualvalue && expected_0 == *actual_0)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}

	}


	// Put tests here
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;
	{
		//Test1
		cout << "Test 1" << endl;
		cout << "create a list and do nothing , size should return 0 and is empty() should return true" << endl;
		List<string>testlist;
		int expectedvalue = 0;
		int actualvalue = testlist.Size();

		bool expect = true;
		bool actual = testlist.IsEmpty();


		if (expectedvalue == actualvalue && expect == actual)
		{
			cout << "Pass" << endl <<endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}

	}
	//Test2
	{
		cout << "Test 2" << endl;
		cout << "create a list pushback A,B,C ,size should return 3 ,isempty() return false " << endl;
		List<string>testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		int expectedvalue = 3;
		int actualvalue = testlist.Size();

		bool expect = false;
		bool actual = testlist.IsEmpty();

		
		if (expectedvalue == actualvalue && expect == actual)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}


	}


	// Put tests here
}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;
	{
		//Test 1  
		cout << "Test 1" << endl;
		cout << "create a list , do nothing , Get(0) should return nullptr" << endl;
		List<string>testlist;
		bool expextedvalue = false;
		bool actualvalue = testlist.Get(0);

		if (expextedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	{
		//Test 2
		cout << "Test 2" << endl;
		cout << "create a list , pushback A,B,C,Get[0,1,2] should return A,B,C" << endl;
		List<string> testlist;

		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		
		string expectedValue_0 = "A";
		string expectedValue_1 = "B";
		string expectedValue_2 = "C";

		string* actualValue_0 = testlist.Get(0);
		string* actualValue_1 = testlist.Get(1);
		string* actualValue_2 = testlist.Get(2);
		
		if (expectedValue_0 == *actualValue_0 &&
			expectedValue_1 == *actualValue_1 &&
			expectedValue_2 == *actualValue_2)
		{
			cout << "Pass" << endl << endl;
		}
		else {
			cout << "Fail" << endl << endl;
		}



	}

	// Put tests here
}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;
	{
		cout << "Test 1." << endl;
		cout << "create a list , do nothing , empty list should return nullptr" << endl;
		List<string>textlist;
		string* expectedvalue = nullptr;
		string* actualvalue = textlist.GetFront();
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	{
		//Test 2
		cout << "Test 2." << endl;
		cout << "create a list and pushback  A and B and C" << endl;
		List<string>testlist;
		testlist.PushBack("A");
		testlist.PushBack("B");
		testlist.PushBack("C");
		string expextedOutput = "A";
		string *actualOutput = testlist.GetFront();
		if (expextedOutput == *actualOutput)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	// Put tests here
}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;
	{
		//test 1
		cout << "Test 1." << endl;
		cout << "create a list , do nothing " << endl;
		List<string>textlist;
		bool expectedvalue = false;
		bool actualvalue = textlist.GetBack();
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	{
		//Test 2
		cout << "Test 2." << endl;
		cout << "create a list and pushback A and B" << endl;
		List<string>list;
		list.PushBack("A");
		list.PushBack("B");
		string expextedOutput = "B";
		string* actualOutput = list.GetBack();
		if (expextedOutput == *actualOutput)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}

	}



	// Put tests here
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;
	cout << endl << "Test 1." << endl;
	// Test 1. //
	List <string>textlist;
	cout << "create a list , do nothing and Get count of A " << endl;
	bool expectedvalue = false;
	bool actualvalue = textlist.GetCountOf("A");
	
	if (expectedvalue == actualvalue)
	{
		cout << "Pass" << endl << endl;
	}
	else
	{
		cout << "Fail" << endl << endl;
	}
	//end Test 1.
	{
		//Test 2 
		cout << endl << "Test 2." << endl;
		List <string>textlist;
		cout << "create a list , pushback 75 T and Get count of T " << endl;
		for (int i = 0; i < 75; i++)
		{
			textlist.PushBack("T");

		}
		int expextedvalue = 75;
		int actualvalue = textlist.GetCountOf("T");

		
		if (expextedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}



	}
	{
		// Test 3
		cout << endl << "Test 3" << endl;
		List <string>textlist;
		cout << "create a list , pushback  A,B,C and Get count of D" << endl;
		textlist.PushBack("A");
		textlist.PushBack("B");
		textlist.PushBack("C");
		

		int expectedvalue = 0;
		int actualvalue = textlist.GetCountOf("D");
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}


	}



	// Put tests here
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;
	{
		//Test 1
		cout << "Test 1" << endl;
		cout << "create a list and do nothing , conttain() should return false " << endl;
		List<string>textlist;
		bool expectedvalue = false;
		bool actualvalue = textlist.Contains("A");
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	{
		//Test 2
		cout << "Test 2 " << endl;
		cout << "create a list and insert A ,B and D , check if it contain C , contain should return false" << endl;
		List<string>list;
		list.Insert(0, "A");
		list.Insert(1, "B");
		list.Insert(2, "D");
		bool expected = false;
		bool actual = list.Contains("C");
		if (expected == actual)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}
	{
		// Test 3
		cout << "Test 3" << endl;
		cout << "create a list and insert 75 U , and contains should return true" << endl;
		List<string>textlist;
		for (int i = 0; i < 75; i++)
		{
			textlist.Insert(i, "T");

		}
		bool expectedvalue = true;
		bool actualvalue = textlist.Contains("T");
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}
	}



	// Put tests here
}

void Tester::Test_Remove()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;
	{
		cout << "Test 1" << endl;
		cout << "create a list , pushback A,B,C then remove at index 0 , remove should return true and get at 0 should return B and get at 1 should return C. size return 2 "<< endl;
		List<string>textlist;
		textlist.PushBack("A");
		textlist.PushBack("B");
		textlist.PushBack("C");
		textlist.RemoveAtIndex(0);
		bool expectedvalue = true;
		bool actualvalue = textlist.RemoveAtIndex(0);

		string expectedValue_0 = "A";
		string expectedValue_1 = "B";

		string* actualValue_0 = textlist.Get(0);
		string* actualValue_1 = textlist.Get(1);
		
		int expextedsize = 2;
		int actualsize = textlist.Size();

		if (expectedvalue == actualvalue && expectedValue_0== *actualValue_0 && expectedValue_1 == *actualValue_1 &&expextedsize== actualsize)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl << endl;
		}



	}
	{
		cout << "Test 2" << endl;
		cout << "create a list , pushback A,B,C then remove at index at 5, remove should return false and size should return 3" << endl;
		List<string>textlist;
		textlist.PushBack("A");
		textlist.PushBack("B");
		textlist.PushBack("C");
		textlist.RemoveAtIndex(5);
		bool expectedvalue = false;
		bool actualvalue = textlist.RemoveAtIndex(5);

		int expextedsize = 3;
		int actualsize = textlist.Size();
		if (expectedvalue == actualvalue &&expextedsize == actualsize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}


	}
	// Put tests here
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;
	{
		// Test 1
		cout << "Test1." << endl;
		cout << "create a list and insert A at  index 0, insert(0,A)should return true,size should return 1 and Get(0) return A" << endl;

		List<string>textlist;
		textlist.PushBack("A");
		bool expectedvalue = true;
		bool actualvalue = textlist.Insert(0,"A");
		cout << "size: " << textlist.Size() << "item at index 0 is " << textlist.Get(0) << endl;
		if (expectedvalue == actualvalue)
		{
			cout << "Pass" << endl << endl;
		}
		else
		{
			cout << "Fail" << endl<<endl;
		}
		
	}
	{
		//Test 2
		cout << "Test. 2" << endl;
		cout << "create a list ,push back A,B,C, insertat 5 ,D should return false size should remain 3" << endl;
		List<string>textlist;
		textlist.PushBack("A");
		textlist.PushBack("B");
		textlist.PushBack("C");
		bool expextedresult = false;
		bool actualresult = textlist.Insert(5, "D");

		int expextedsize = 3;
		int actualsize = textlist.Size();
		if (expextedsize == actualsize && expextedresult== actualresult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}

	}

		

	


	// Put tests here
}
