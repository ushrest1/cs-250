#ifndef _STACK_HPP
#define _STACK_HPP
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
		m_itemCount = 0;
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;

    }

    void Push( const T& newData )
    {
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;

		if (m_ptrLast == nullptr)
		{
			// Point the first & last ptrs to the new node
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}

		// List has at least one item
		else
		{
			// Point the last node's ptrNext to the new node
			m_ptrLast->ptrNext = newNode;
			newNode->ptrPrev = m_ptrLast;
			m_ptrLast = newNode;
		}
		m_itemCount++;

    }

    T& Top()
    {
		if (m_ptrLast == nullptr)
		{
			throw  CourseNotFound ("CourseNotFound");   // placeholder
		}
		return m_ptrLast->data;
    }

    void Pop()
    {
		
		if (m_ptrLast == nullptr)
		{
			return;
		}
		else if (m_ptrFirst == m_ptrLast)
		{
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else
		{
			Node<T>* newPtr = new Node<T>;
			newPtr = m_ptrLast->ptrPrev;
			newPtr->ptrNext = nullptr;
			
			m_ptrLast = newPtr;
			m_itemCount--;
			
			
		}


    }

    int Size()
    {
		return m_itemCount;    // placeholder
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
