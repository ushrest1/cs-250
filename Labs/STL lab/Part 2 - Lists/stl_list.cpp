// Lab - Standard Template Library - Part 2 - Lists
// UTTAM, SHRESTHA

#include <iostream>
#include <list>
#include <string>
using namespace std;
void DisplayList(list < string >& states)
 {
	 for (list < string >::iterator it = states.begin();
		 it != states.end();
		 it++)
		{
		 cout << *it << " \t ";
		}
	 cout << endl;
	 }

int main()
{
	list<string> states;
	bool done = false;
	while (!done)
	{
		cout << endl << "MAIN MENU" << endl;
		cout << "state size:" << states.size() << endl;
		cout << "1. Add a new state to front"
			<< "2. Add a new state to back"
			<< "3.pop front state"
			<< "4.pop back state"
			<< "5. Quit" << endl;
		int choice;
		cin >> choice;

		if (choice == 1)
		{
			string newStates;
			cout << "enter new state name: ";
			cin >> newStates;
			states.push_front(newStates);
		}
		else if (choice == 2)
		{
			string newStates;
			cout << "enter new state name:";
			cin >> newStates;
			states.push_front(newStates);

		}
		else if (choice == 3)
		{
			states.pop_front();
			
		}
		else if (choice == 4)
		{
			states.pop_back();
			
		}
		else
		{
			done = true;
		}

		



	}
	cout << "\n\n Normal list:" << endl;
	DisplayList(states);

	cout << "\n\n Reverse list" << endl;
	states.reverse();
	DisplayList(states);

	cout << "\n\n sort list :" << endl;
	states.sort();
	DisplayList(states);

	cout << "\n\n  Sort-reverse list:" << endl;
	states.reverse();
	DisplayList(states);


    cin.ignore();
    cin.get();
    return 0;
}
