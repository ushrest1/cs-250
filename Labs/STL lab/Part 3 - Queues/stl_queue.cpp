// Lab - Standard Template Library - Part 3 - Queues
// FIRSTNAME, LASTNAME

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float>myqueue;
	bool done = false;
	while (!done)
	{
		cout << "----------" << endl;
		cout << "1.Do you want to do next transaction "<<
			 "2.quit" << endl;
		int choice;
		cin >> choice;

		if (choice == 1)
		{
			cout << "enter amount:";
			float amount;
			cin>> amount;
			myqueue.push(amount);
		}
		else 
		{
			done = true;
		}
		
	
	}
	float balance= 0;
	
	while (!myqueue.empty())
	{
		 balance += myqueue.front();
		 myqueue.pop();
	}
	cout << "total balance:" << balance << '\n';


    cin.ignore();
    cin.get();
    return 0;
}
